Troubleshooting
---------------

If you encounter any problems you can report them to
support@quantumdetectors.com.

Some potential fixes you can try yourself will also be added below.


Stage appears to be stuck in application
========================================

Symptoms
########

The current state is stuck in "Moving", but the current stage position is not
updating.

.. image:: ../images/troubleshooting_stuck_in_moving_state.png
    :width: 600
    :alt: Stuck in moving state

Possible fix
############

Restart the GUI application. The application can attempt to recover from this
problem automatically when it is started.

If the problem persists, contact support using the email at the top of this
page.
