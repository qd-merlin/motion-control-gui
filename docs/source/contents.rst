Contents
========

.. toctree::
    :caption: Introduction

    Introduction <index>

.. toctree::
    :caption: User guide

    user_guide/main_screen
    user_guide/info_window
    user_guide/settings_window
    user_guide/testing_window

.. toctree::
    :caption: Troubleshooting

    troubleshooting/troubleshooting

.. toctree::
    :caption: Reference

    reference/changelog
