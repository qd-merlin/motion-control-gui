Settings window
---------------

This window can be used to view and change some of the controller settings used
to move the stage.

To modify one or more of the values you can change the values inside the text
boxes and then press the "Update values" button. This can be useful to find the
correct insertion distance for the detector.

Values will be restored to their defaults from the `settings.json` the next time
the GUI application is launched. If you want a permanent change to a setting
then you should update this configuration file instead.

.. warning::
    Take care modifying these values. You can cause issues with the motion if
    these are not set properly and should only be changed with the support or
    advice from Quantum Detectors.

.. note::
    You can click outside of the pop-up info window, but inside the application
    window, to go back to the main screen.

.. image:: ../images/settings_window.png
    :width: 600
    :alt: Settings window
