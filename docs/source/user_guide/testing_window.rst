Testing window
--------------

The test pop-up window can be used to perform cycling tests of the stage. This
is used to confirm that the stage can be inserted and retracted without any
issues repeatedly.

.. note::
    You can click outside of the pop-up info window, but inside the application
    window, to go back to the main screen. If a test is in progress it will
    continue running in the background.

Running a test
==============

To set up a test enter the number of cycles and delay in seconds in between
every insertion and retraction (green box). The estimated time will update
based on these values (orange box). You can start the test by pressing the
"Cycle" button.

.. image:: ../images/cycle_window_setup.png
    :width: 600
    :alt: Cycle test setup

Whilst the test is running the current test progress will update (orange box).
You will also see the stage state update in the background (green box).

.. image:: ../images/cycle_window_running.png
    :width: 600
    :alt: Cycle test running

Eventually the test will finish and the stage can be then continued to be
controlled as normal. The test will finish with the stage in the retracted
position if successful.

.. image:: ../images/cycle_window_finished_test.png
    :width: 600
    :alt: Cycle test finished

Aborting a test
===============

If you want to stop the test early press the "Abort" button. This will stop
new commands from being sent to insert or retract the stage, but the last
command sent will still complete (e.g. if inserting, then the stage will end up
at the inserted position).

.. image:: ../images/cycle_window_aborted.png
    :width: 600
    :alt: Cycle test aborted
