Info window
-----------

The information pop-up window shows the current version of the software.

.. note::
    You can click outside of the pop-up info window but inside the application
    window to go back to the main screen.

.. image:: ../images/info_window.png
    :width: 600
    :alt: Information window
