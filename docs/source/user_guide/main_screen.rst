Main application screen
-----------------------

The main application window appears when the application is started.

.. image:: ../images/main_screen_first_open.png
    :width: 600
    :alt: Main application window

The left hand side of the screen hosts the control buttons for moving or
stopping the stage (highlighted in red). Highlighted in blue are the
current state of the stage, the requested state of the stage and the state
of the interlock signals. Additional information and test windows are
accessed from the top-right of the window (shown in green).

.. image:: ../images/main_screen_annotated.png
    :width: 600
    :alt: Main application window - annotated


Inserting the stage
===================

Press the "Insert" button on the main screen to start inserting the stage. If
the hardware interlock is safe, the requested state will update to "Inserted",
the current state will update to "Moving" and the current position will start
increasing towards the insertion position. All buttons except the "Stop" button
will become disabled during motion.

.. image:: ../images/main_screen_inserting.png
    :width: 600
    :alt: Inserting the stage

Eventually the stage will reach the insertion position and the current state
will read "Inserted", and the current position will match the insert position.
All motion-related buttons will be enabled again to move the stage.

.. image:: ../images/main_screen_inserted.png
    :width: 600
    :alt: Stage is inserted


Retracting the stage
====================

Press the "Retract" button on the main screen to start retracting the stage.
The requested state will update to "Retracted", the current state will update
to "Moving" and the current position will start decreasing to 0 mm. All buttons
except for "Stop" will become disabled during motion.

.. note::
    If the hardware interlock becomes unsafe then retraction will start
    automatically without user input, even without the GUI application
    running.

.. image:: ../images/main_screen_retracting.png
    :width: 600
    :alt: Retracting the stage

Eventually the stage will reach the retracted position and the current state
will read "Retracted". The current position will settle at 0 mm. All buttons
become available again for controlling the stage.

.. note::
    The position will overshoot 0 temporarily as the stage will home off of
    the negative limit switch and then move off the switch by a small amount.

.. image:: ../images/main_screen_retracted.png
    :width: 600
    :alt: Stage is retracted


Moving the stage to standby position
====================================

Press the "Standby" button on the main screen to start moving the stage to the
standby position. If the interlock is safe, the requested state will update to
"Standby", the current state will update to "Moving" and the current position
will start changing in the direction of the standby position. All buttons except
"Stop" will become disabled during motion.

.. note::
    The stage will move either inwards or outwards depending on its current
    position.

.. image:: ../images/main_screen_moving_to_standby.png
    :width: 600
    :alt: Moving the stage to standby

Eventually the stage will reach the standby position and the current state will
read "Standby". All buttons for controlling the stage become available again.

.. image:: ../images/main_screen_at_standby.png
    :width: 600
    :alt: Stage is at standby position


Stopping the stage during motion
================================

Any motion due to user requests can be aborted at any time.

Press the "Stop" button and the stage will come to a quick stop and the current
state will read "Stopped". The stage will stay at this position until the user
requests it to move again (or the interlock causes a retraction).

.. image:: ../images/main_screen_stopped.png
    :width: 600
    :alt: Stage is stopped
