Change log
==========

The following is a list of notable changes to the GUI software and backend
Python module.

Contact support@quantumdetectors.com for an updated version of the software.

.. include:: ../../../CHANGELOG.rst
    :start-line: 8
