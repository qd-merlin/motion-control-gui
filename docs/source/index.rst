QD motion control application
-----------------------------

This is a user guide for using the Python application for controlling the
retractable detector stages used by Quantum Detectors.

Navigation is available on the left sidebar of this page and includes:

- A basic user guide
- A troubleshooting guide

If you have any questions do not hesitate to contact our support team at
support@quantumdetectors.com.

Index
=====

* :ref:`search`
