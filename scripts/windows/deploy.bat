@echo off
:: Launch the PowerShell script from this batch file to get around Execution Policy
:: warnings.

Powershell.exe -executionpolicy bypass -File "%~dp0deploy.PS1"  %*
exit %ERRORLEVEL%
