# Script parameters
param (
    [switch]$offline = $false,   # Whether to create offline deployment package
    [switch]$noclean = $false    # Whether to clean up virtual environment after (used by CI)
)

Write-Output "==========================================================="
Write-Output "Creating Motion Control deployment"
Write-Output "==========================================================="


#================================================
# Environment variables
#================================================

# Version and module name
Set-Variable -Name "MODULE_NAME" -Value "motion_control_gui"
$VERSION = Get-Content .\VERSION.txt

# Package name
Set-Variable -Name "PACKAGE_NAME" -Value "$MODULE_NAME-$VERSION-Windows"
if ($offline -eq $true) {
    # Get the Python Version to add to package name
    [string] $output = Invoke-Expression -Command "python -V 2>&1"
    $PYTHON_VERSION = ($output -Split " ")[1]

    if ($PYTHON_VERSION[0] -ne "3") {
        Write-Output "- ERROR: did not get valid Python version: $PYTHON_VERSION"
        Write-Output "  => Expected version matching 3.x.x"
        Write-Output "  => Check Python is installed with 'python -V'"
        return
    }

    # Create offline package name with Python version used to generate it
    $PACKAGE_NAME = $PACKAGE_NAME + "-Python_$PYTHON_VERSION-OFFLINE"
}

# Input paths
Set-Variable -Name "DEPLOYMENT_SOURCE_DIR" -Value ".\deployment\windows"
Set-Variable -Name "DEPENDENCY_WHEEL_DIR" -Value ".\dependencies"

# Output paths
Set-Variable -Name "DEPLOY_DIR" -Value ".\deploy"
Set-Variable -Name "PACKAGE_DIR" -Value "$DEPLOY_DIR\$PACKAGE_NAME"
Set-Variable -Name "PACKAGE_ZIP" -Value "$PACKAGE_DIR.zip"
Set-Variable -Name "WHEEL_DIR" -Value "$PACKAGE_DIR\wheels"
Set-Variable -Name "SCRIPTS_DIR" -Value "$PACKAGE_DIR\scripts"
Set-Variable -Name "DEPLOY_VENV" -Value ".\deploy_venv"

Write-Output "- Module name: $MODULE_NAME"
Write-Output "- Package version: $VERSION"
Write-Output "- Package directory: $PACKAGE_DIR"
Write-Output "- Offline deployment support: $OFFLINE"


#================================================
# Create the output paths
#================================================

Write-Output "- Creating output paths"

# Remove any existing deployment directories with same name/version
if (Test-Path $PACKAGE_DIR) { Remove-Item -Path $PACKAGE_DIR -Recurse }
if (Test-Path $PACKAGE_ZIP) { Remove-Item -Path $PACKAGE_ZIP }

# Create the directory structure
if ((Test-Path $DEPLOY_DIR) -eq $false) { $null = New-Item -Path $DEPLOY_DIR -ItemType Directory }
$null = New-Item -Path $PACKAGE_DIR -ItemType Directory
$null = New-Item -Path $WHEEL_DIR -ItemType Directory

# Copy deployment files
Copy-Item -Recurse -Path "$DEPLOYMENT_SOURCE_DIR\images" -Destination "$PACKAGE_DIR\images"
Copy-Item -Recurse -Path "$DEPLOYMENT_SOURCE_DIR\scripts" -Destination "$SCRIPTS_DIR"
Copy-Item -Recurse -Path "$DEPENDENCY_WHEEL_DIR\*" -Destination "$WHEEL_DIR"
Copy-Item "$DEPLOYMENT_SOURCE_DIR\install.bat" -Destination "$PACKAGE_DIR"
if ($offline -eq $true)
{
    Copy-Item "$DEPLOYMENT_SOURCE_DIR\install_offline.bat" -Destination "$PACKAGE_DIR"
    $null = New-Item -Path $SCRIPTS_DIR\PYTHON_VERSION.txt
    Set-Content $SCRIPTS_DIR\PYTHON_VERSION.txt $PYTHON_VERSION
}


#================================================
# Create the module Wheel
#================================================

Write-Output "- Building module Wheel"

try {
    $null = Invoke-Expression -Command "python -m venv $DEPLOY_VENV 2>&1"
    $null = Invoke-Expression -Command "$DEPLOY_VENV\Scripts\Activate.ps1 2>&1"
    $null = Invoke-Expression -Command "pip install --upgrade pip 2>&1"
    $null = Invoke-Expression -Command "pip install build 2>&1"
    $null = Invoke-Expression -Command "python -m build -w --outdir $WHEEL_DIR"
    $null = Invoke-Expression -Command "deactivate"
}
catch {
    Write-Output "- Failed to build Wheel"
    try{
        Write-Output "- Cleaning up virtual environment"
        # Try and deactivate the active environment in case we failed part way
        # through installation.
        $null = Invoke-Expression -Command "deactivate"
        if ($noclean -eq $false) {
            Write-Output "- Cleaning up virtual environment"
            Remove-Item -Path $DEPLOY_VENV -Recurse
        }
    }
    catch {
        # Catch the exception in the case the venv was never active
    }
    exit 1
}

if ($noclean -eq $false) {
    Write-Output "- Cleaning up virtual environment"
    Remove-Item -Path $DEPLOY_VENV -Recurse
}


#================================================
# Download Wheels for offline install
#================================================

if ($offline -eq $true)
{
    Write-Output "- Downloading dependency Wheels for offline install"

    Set-Variable -Name "OFFLINE_VENV" -Value ".\offline_venv"
    Set-Variable -Name "WHEELS_REQUIRED" -Value "$SCRIPTS_DIR\wheels_required.txt"

    if (Test-Path $WHEELS_REQUIRED) { Remove-Item -Path $WHEELS_REQUIRED }

    try {
        # Use a virtual environment to generate the requirements file
        $null = Invoke-Expression -Command "python -m venv $OFFLINE_VENV 2>&1"
        $null = Invoke-Expression -Command "$OFFLINE_VENV\Scripts\Activate.ps1 2>&1"
        $null = Invoke-Expression -Command "pip install --upgrade pip 2>&1"

        # Install the motion_control and motion_control_gui modules
        Get-ChildItem $DEPENDENCY_WHEEL_DIR -Filter motion_control-*.whl | ForEach-Object {
            $WHEEL_FILE = $_.FullName
            $null = Invoke-Expression -Command "pip install $WHEEL_FILE 2>&1"
        }
        Get-ChildItem $WHEEL_DIR -Filter motion_control_gui-*.whl | ForEach-Object {
            $WHEEL_FILE = $_.FullName
            $null = Invoke-Expression -Command "pip install $WHEEL_FILE 2>&1"
        }

        # Generate requirements file without this module installed (leaving dependencies)
        $null = Invoke-Expression -Command "pip uninstall -y motion_control 2>&1"
        $null = Invoke-Expression -Command "pip uninstall -y motion_control_gui 2>&1"
        $null = Invoke-Expression -Command "pip freeze > $WHEELS_REQUIRED"

        # Download the Wheels into the Wheel directory
        $null = Invoke-Expression -Command "pip download -r $WHEELS_REQUIRED -d $WHEEL_DIR 2>&1"

        $null = Invoke-Expression -Command "deactivate"
    }
    catch {
        Write-Output "- Failed to download dependency Wheels"
        try{
            Write-Output "- Cleaning up dependency virtual environment"
            # Try and deactivate the active environment in case we failed part way
            # through installation.
            $null = Invoke-Expression -Command "deactivate"
            if ($noclean -eq $false) {
                Write-Output "- Cleaning up virtual environment"
                Remove-Item -Path $OFFLINE_VENV -Recurse
            }
        }
        catch {
            # Catch the exception in the case the venv was never active
        }
        exit 1
    }

    if ($noclean -eq $false) {
        Write-Output "- Cleaning up dependency virtual environment"
        Remove-Item -Path $OFFLINE_VENV -Recurse
    }
}


#================================================
# Turn the package into an archive
#================================================

Write-Output "- Creating package archive"

try { 
    Set-Location $DEPLOY_DIR
    $null = Invoke-Expression -Command "tar.exe -a -c -f $PACKAGE_NAME.zip $PACKAGE_NAME"
}
catch {
    Write-Output "- Error creating package archive"
    Set-Location ..
    exit 1
}

# Return back to top directory
Set-Location ..

Write-Output "- Cleaning up"
Remove-Item -Path $PACKAGE_DIR -Recurse
