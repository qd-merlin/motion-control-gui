#!/bin/bash

#==========================================================
# Release assets
#==========================================================

# Deploy jobs provide us files containing 1 package per line
# as name and url
linux_package_file=./linux-packages.md
windows_package_file=./windows-packages.md

# Linux assets
if [ -f $linux_package_file ]; then
    echo "Found Linux packages"

    while read line; do
        # Parse as array
        entry=($line)
        LINK="{\"name\":\"${entry[0]}\", \"url\":\"${entry[1]}\"}"

        # Create or append to list of links
        if [ -z "$LINKS" ]; then
            LINKS=$LINK
        else
            LINKS="$LINKS,$LINK"
        fi

    done <<< "$(cat $linux_package_file)"
fi

# Windows assets
if [ -f $windows_package_file ]; then
    echo "Found Windows packages"

    dos2unix $windows_package_file
    while read line; do
        # Parse as array
        entry=($line)
        LINK="{\"name\":\"${entry[0]}\", \"url\":\"${entry[1]}\"}"

        # Create or append to list of links
        if [ -z "$LINKS" ]; then
            LINKS=$LINK
        else
            LINKS="$LINKS,$LINK"
        fi

    done <<< "$(cat $windows_package_file)"
fi

echo "Got asset links: $LINKS"


#==========================================================
# Create release
#==========================================================

# Perform release
release-cli create --name "Release $CI_COMMIT_TAG" --tag-name "$CI_COMMIT_TAG" --description release_notes.md --assets-link="[$LINKS]"
