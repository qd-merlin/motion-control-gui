#!/usr/bin/bash

# Colours
CYAN="\033[0;36m"
GREEN="\033[1;32m"
YELLOW="\033[1;33m"
RED="\033[0;31m"
NOCOL="\033[0m"

PACKAGE_TITLE="Motion control GUI"

#================================================
# Script options
#================================================

debug_flag=0

options=":dh"

while getopts ${options} flag
do
    case ${flag} in
        d)
            debug_flag=1
            ;;
        h)
            echo ""
            echo -e "==========================================================="
            echo "$PACKAGE_TITLE deploy script"
            echo -e "==========================================================="
            echo ""
            echo "This script is used to build the deployment package."
            echo ""
            echo "Choose from the following options:"
            echo ""
            echo "    -d : build with debug output"
            echo "    -h : print this help message and exit"
            echo ""
            echo -e "==========================================================="
            echo ""
            exit 0
            ;;
        ?)
            echo "ERROR: Invalid option '${OPTARG}'. Type -h for help"
            exit 1
            ;;
    esac
done


echo "==========================================================="
echo "Creating $PACKAGE_TITLE deployment package"
echo "==========================================================="
echo ""

# Redirection for debug or minimal output
if [ $debug_flag -eq 1 ]; then
    echo -e "${GREEN}[INFO]${NOCOL} Enabling debug output"
    echo ""
    REDIRECT=/dev/stdout
else
    REDIRECT=/dev/null
fi


#================================================
# Packaging configuration
#================================================

module_name="motion_control_gui"
module_version=$(cat "./VERSION.txt")
package_name="$module_name-$module_version-Linux"

echo -e "Package name: ${CYAN}$package_name${NOCOL}"
echo ""

deploy_directory="./deploy"
package_directory="$deploy_directory/$package_name"
package_archive="$package_name.tar.gz"

docs_directory=$package_directory/docs
images_directory=$package_directory/images
service_directory=$package_directory/service
shortcut_directory=$package_directory/shortcut
wheel_directory=$package_directory/wheels

dependency_wheel_directory="./dependencies"
source_deployment_directory="./deployment/linux"
source_docs_dir="./docs/source"
source_images_directory=$source_deployment_directory/images
source_service_directory=$source_deployment_directory/service
source_shortcut_directory=$source_deployment_directory/shortcut


#================================================
# Create package paths
#================================================

echo -e "${CYAN} - Creating package directory${NOCOL}"

if [ -d $package_directory ]; then
    echo -e "${YELLOW}[WARNING]${NOCOL} Deleting existing package directory: $package_directory"
    rm -rf $package_directory
fi

mkdir -p $package_directory
mkdir -p $docs_directory
mkdir -p $images_directory
mkdir -p $service_directory
mkdir -p $shortcut_directory
mkdir -p $wheel_directory

# Copy pre-made package contents
cp $dependency_wheel_directory/*.whl $wheel_directory
cp $source_deployment_directory/*.sh $package_directory
cp $source_deployment_directory/*.txt $package_directory
cp -r $source_images_directory/* $images_directory
cp -r $source_service_directory/* $service_directory
cp -r $source_shortcut_directory/* $shortcut_directory


#================================================
# Build module Wheel and docs
#================================================

echo -e "${CYAN} - Building module Wheel and docs${NOCOL}"

deploy_venv="./deploy_venv"

# Create the Wheel and docs from a new virtual environment
python3.11 -m venv $deploy_venv > $REDIRECT
source "$deploy_venv/bin/activate" > $REDIRECT
pip install --upgrade pip setuptools build sphinx sphinx-rtd-theme > $REDIRECT
python -m build -w --outdir $wheel_directory > $REDIRECT
sphinx-build -b html $source_docs_dir $docs_directory > $REDIRECT
deactivate > $REDIRECT
rm -rf $deploy_venv > $REDIRECT

# Check the Wheel was created
wheel_was_built=$(find $wheel_directory -name "$module_name*.whl" | grep .)
if [ ! $wheel_was_built ]; then
    echo -e "${RED}[ERROR]${NOCOL} Failed to build module wheel"
    exit 1
else
    echo -e "${GREEN} - Built module wheel${NOCOL}"
fi


#================================================
# Create package archive
#================================================

echo -e "${CYAN} - Creating package archive${NOCOL}"

echo $package_directory
echo $package_name

cd $deploy_directory
tar czf $package_archive $package_name
rm -rf $package_name
cd -

echo -e "${GREEN} - Package created: $package_archive${NOCOL}"
