#!/bin/bash

#==========================================================
# Generate release notes
#==========================================================

# Version from VERSION.txt
VERSION=$(cat ./VERSION.txt)

echo "Generating release notes for version '$VERSION'"

CHANGELOG_FILE=./CHANGELOG.rst

if [ -z $VERSION ]; then
    # Generate dummy file and exit
    echo "WARNING: skipping release notes as no version set"
    echo "$release_notes" > release_notes.md
    exit 1
fi

# Find start and last line numbers
found_version=0
found_version_and_heading=0
end_of_version=0
line_number=1
start_line_number=0
end_line_number=0
if [ -f $CHANGELOG_FILE ]; then
    echo " - Found change log"

    while read line; do

        # Check if we are done
        if [ $end_of_version -eq 1 ]; then
            echo " - Finished searching for release notes"
            break
        fi

        # Check if we already entered versioning info
        if [ $found_version_and_heading -eq 1 ]; then
            # Check for end by looking for next version heading
            if [[ $line == *"---"* ]]; then
                echo " - Found next version heading"
                end_of_version=1
                end_line_number=$(( line_number - 2))
            fi

        else
            # Otherwise check if we already found version on previous line
            if [ $found_version -eq 1 ]; then
                if [[ $line == *"---"* ]]; then
                    echo " - Found version heading"
                    found_version_and_heading=1
                    start_line_number=$(( line_number + 1))
                else
                    # Missing heading so just coincidence
                    echo " - Didn't find heading after version - resetting"
                    found_version=0
                fi
            else
                # Otherwise look for matching version string
                if [[ $line == *"$VERSION"* ]]; then
                    echo " - Found version in line $line"
                    found_version=1
                fi
            fi
        fi

        prev_line=$line
        line_number=$(( line_number + 1))

    done <<< "$(cat $CHANGELOG_FILE)"

fi

# Exit early if we didn't find any notes
if [ $found_version_and_heading -eq 0 ]; then
    echo "WARNING: no release notes found for $VERSION"
    echo "$release_notes" > release_notes.md
    exit 1
fi

# Check if we didn't find the end of the release notes
if [ $end_line_number -eq 0 ]; then
    # Set to end of file
    end_line_number=$line_number
fi

echo "   - Start line: $start_line_number"
echo "   - End line: $end_line_number"
echo "   - Total lines: $line_number"

# Only get the lines we found
release_notes=$(sed -n "${start_line_number},${end_line_number}p"  < CHANGELOG.rst)

echo ""
echo "========================="
echo "Generated release notes:"
echo "========================="
echo "$release_notes"
echo "========================="
echo ""

# Create the file
echo "$release_notes" > release_notes.rst
# Convert to Markdown
pandoc -o release_notes.md release_notes.rst
