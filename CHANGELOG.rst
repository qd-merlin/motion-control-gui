Changelog
=========

Major changes will be documented in this file.

The format is based on `Keep a Changelog
<https://keepachangelog.com/en/1.0.0/>`_, and this project adheres to `Semantic
Versioning <https://semver.org/spec/v2.0.0.html>`_.

3.4.3
-----

Added:

- CI/CD release mechanism

Changed:

- Updated to motion_control v1.3.4

  - Updated to new ZeroMQ control and configure interface

- Changed package name to include platform

Fixed:

- Deploy script now looks for correct name pattern for built wheel
- Desktop icon now set properly when pinned in Linux


3.4.2
-----

Changed:

- Update motion_control to v1.3.3:

  - Retract speed is now dynamic to match insert speed (except in case of CETA
    legacy interlock which will always retract as fast as possible)
  - Acceleration and speed reduced for RDP stages
  - CLI tools are now simpler to use and prompt when creating a config file
    to generate and upload the motion program

- Add button on info window to open documentation in browser


3.4.1
-----

Changed:

- Update motion_control to v1.3.1 to fix CETA power interruption logic


3.4.0
-----

Added:

- Support for Linux with deploy and install scripts

Changed:

- Updated motion_control to v1.3.0 for Linux support


3.3.3
-----

Changed:

- Updated to motion_control v1.2.5 for support for the Timepix 4 RDP prototype
  using the Haydon-Kerk motor.


3.3.2
-----

Changed:

- Updated to motion_control 1.2.3 for legacy pneumatic interlock support
- Moved try_parse_value_string from motion_control module to this module as
  only used by this GUI


3.3.1
-----

Added:

- Shortcut now added to upload custom programs to the installation folder
- Taskbar icon

Changed:

- Updated to motion_control 1.2.2 which separates out the motion program
  generation from the uploading to the controller.
- No longer minimise console window if explicitly launching GUI with
  console enabled
- Main screen now only initialises the GUI fields that aren't polled by the
  poll thread on the initial connection.

Fixed:

- Disabled multi-touch emulation with the mouse in the Kivy configuration. This
  prevents the red dots from appearing with right clicks.


3.3.0
-----

Added:

- GUI script entry point for Windows. This entry point can be used to launch
  the GUI without a console enabled
- PyInstaller executable for the GUI application. This builds the GUI entry
  point into a single executable. This is not currently used for deployment
  as there are still issues to resolve with signing the executable.

Changed:

- Updated to motion_control 1.2.0
- Renamed the existing script entry point to `motion_control_gui_console`. This
  keeps the console window (but gets minimised at the start) so log messages can
  be seen for debugging purposes. A shortcut for this is added in the top-level
  directory `C:\quantumdetectors\motion-control` when installed.
- Now postpone the initialisation of the GUI values until the motion control
  interface is connected. This is done in a separate thread to avoid hanging the
  GUI thread and blocking user interaction. This applies to the settings and test
  modal windows as well as the main window.


3.2.4
-----

Changed:

- Bumped Motion_control to apply changes to RDP defaults


3.2.3
-----

Changed:

- Bumped motion_control to 1.1.2 to improve ZeroMQ interface handling
- Now returns blocked when requesting move to stanby fails


3.2.2
-----

Changed:

- Bumped motion control to 1.1.1: Default insert speed now medium
  

3.2.1
-----

Changed:

- Bumped motion control to 1.1.0: Added local program upload exe


3.2.0
-----

Changed:

- Renamed Python module to `motion_control_gui`
- Refactored motion controller logic into separate Python module with original
  name of this module `motion_control`
- Updated `motion_control` interface
- Updated install and deploy scripts to include backend module Wheel

Fixed:

- Paths used in installer script now wrapped in single quotes to fix issue with
  spaces being present in the directory path where the package is downloaded to
- MSVC runtime library now added as a dependency to backend module in case that
  host PC has no runtime libraries already installed (required for gclib).


3.1.9
-----

Fixed:

- Added a new state to the CETA interlock motion program used when the interlock causes the stage
  to retract. The stop button is disabled on the GUI when this state is active.


3.1.8
-----

Fixed:

- Bug where the config is not created in the first instance if the 'MotionControl' folder didn't exist
  Folder now generated in the create_config program. 

Added:

- Legacy CETA interlock template (uses different digital inputs to current CETA).

Changed:

- Wording on mainscreen to be clearer that QD motion is being Vetoed.


3.1.7
-----

Added:

- Check after failed upload of program, to the controller, if the E-Stop is connected.
  Will prompt user to check E-Stop is fitted when ABRT pin is low.

3.1.6
-----

Added:

- Offline deployment package creation. Supports creating an offline deployment
  package which includes all dependencies downloaded so it can be installed on
  a PC without internet access.
- Offline deployment package uploaded to package registry for the python version
  on the CI server.

Changed:

- Migrated deploy batch file to PowerShell


3.1.5
-----

Fixed:

- Now tries to detect Microsoft app execution aliases when running install script
  and returns an error if detected.
- Better handling of errors during virtual environment set up and Wheel install


3.1.4
-----

Fixed:

- Incorrect variable names in Pneumatic template


3.1.3
-----

Fixed:

- Incorrect variable names in CETA template


3.1.2
-----

Added:

- Now writes the last uploaded file to `C:\ProgramData\MotionControl` for reference


3.1.1
-----

Added:

- Default insert, standby and maximum positions based on detector type. Split
  Merlin DetectorType into Merlin1R and Merlin4R to handle the different values

Changed:

- Changed desktop shortcut name to "QD motion control" for readability
- Renamed "motion_control/views" to "motion_control/gui"
- Detector type enum now starts from 1

Fixed:

- Entering an invalid IP address in the configuration creator script no longer
  crashes the script


3.1.0
-----

Added:

- Shortcut icon used for the desktop shortcut
- ZeroMQ client interface for communicating with MotionLink ZeroMQ server
  (used for the Thermo Fisher UDI server)

Changed:

- Refactored the `update_interlock` module and moved logic into lower-level
  modules
- Refactored motion link interface and controller classes ready for ZeroMQ
  interface
- Simulator now moved to own class with a background thread to handle motion
  instead of relying on position polls to move. Also now observes speed setting
  when simulating motion.
- Moved window hiding behaviour to GUI class and only do it when not in debug
  mode.
- Moved entry points to CLI folder


3.0.1
-----

Changed:

- Console window now hides after successful connection to motion controller to
  make connection errors visible to user.


3.0.0
-----

Added:

- CI/CD build and deployment
- Project metadata files
- Loggable interface with module-level logging
- Attempt to recover from bad motion program state during initialisation
- CLI interface to check the config file: `check_config`
- CLI interface to create a config file: `create_config`
- Entry point for launching GUI: `motion_control`
- Entry point for updating motion program: `update_interlock`

Changed:

- Reconfigured project as a Python module using pyproject.toml
- Refactoring of modules to make classes more abstract
- Set Kivy logging level to warning
- Stop button can now be pressed during movement to standby
- MotionLink class is now stateless except for debug mode
- Integrating `gclib` wrapper and libraries into the Python module
  - The Python wrapper is skipped for linting and formatting checks

Fixed:

- GUI layout of modal windows
