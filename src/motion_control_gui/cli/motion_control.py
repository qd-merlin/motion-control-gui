"""
Motion control CLI module

Provides the entry point for launching the GUI application
"""

import sys

from motion_control_gui import get_module_version_string
from motion_control_gui.app import MotionControlApp


def main():
    """Launch the GUI application.

    Can run with --debug for simulation mode or --version to get the module
    version and exit.
    """

    # Check if we want simulation mode
    debug = False
    for arg in sys.argv:
        if "DEBUG" in arg.upper():
            debug = True
        elif "VERSION" in arg.upper():
            print(get_module_version_string())
            return

    args_dict = {"debug": debug}

    # Create the application
    MotionControlApp(**args_dict).run()


if __name__ == "__main__":
    if "--version" in sys.argv:
        print(get_module_version_string())

    # Allow launch from python -m
    main()
