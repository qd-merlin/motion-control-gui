import os

os.environ["KCFG_KIVY_LOG_LEVEL"] = "warning"

try:
    import importlib.metadata

    __version__ = importlib.metadata.version("motion_control_gui")
except ImportError:  # pragma: no cover
    # Not supported in Python <=3.7
    __version__ = "unknown"  # pragma: no cover


def get_module_version_string() -> str:
    """Get a nicely formatted string including the module version.

    Returns:
        str: Formatted string including module version
    """
    return (
        "===========================================\n"
        "Motion Control GUI application\n"
        "===========================================\n"
        "Quantum Detectors motion control GUI\n"
        "application.\n\n"
        f"Module version: {__version__}\n\n"
        "Written by Quantum Detectors\n"
        "===========================================\n"
    )
