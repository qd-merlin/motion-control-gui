"""
General utility module

Contains generic utility functions
"""

from typing import Any, Optional


def try_parse_value_string(value: str, value_type: type) -> Optional[Any]:
    """Try and parse a value string as the requested type. Returns None if failed
    or the parsed value.

    Args:
        value (str): String containing the value to parse
        value_type (type): Type to parse as

    Returns:
        Any: Value if parsed successfully, otherwise None
    """
    try:
        return value_type(value)
    except Exception:
        return None
