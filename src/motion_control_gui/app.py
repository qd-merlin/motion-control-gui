"""
Application module

Launches the Kivy application and creates the main window.

Some of the GUI layout and behaviour is stored in .kv Kivy files
which are parsed by Kivy at runtime.
"""

import ctypes
import os
import platform
from pathlib import Path

from kivy.app import App
from kivy.config import Config
from kivy.lang import Builder

from motion_control_gui import __version__
from motion_control_gui.main import mainscreen
from motion_control_gui.main.actionbuttons import ActionButtons
from motion_control_gui.main.labels import Labels
from motion_control_gui.main.mainscreen import MainScreen
from motion_control_gui.main.statusfields import StatusFields
from motion_control_gui.modals.info import infowindow
from motion_control_gui.modals.settings import settingswindow
from motion_control_gui.modals.test import testwindow
from motion_control_gui.static import styles

if platform.system() == "Windows":
    # Report to Windows so that we can set the taskbar icon
    myappid = f"quantumdetectors.motion_control.gui_app.{__version__}"
    assert hasattr(
        ctypes, "windll"
    ), "No WinDLL attribute - this is only valid for Windows platform"
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
elif platform.system() == "Linux":
    os.environ["SDL_VIDEO_X11_WMCLASS"] = "motion_control_gui"

# Set Kivy configuration
Config.set(
    "kivy",
    "window_icon",
    f"{Path(os.path.dirname(__file__)) / 'static/images/mc_logo.png'}",
)
Config.set("input", "mouse", "mouse, disable_multitouch")


# These classes are used in Kivy files but not directly in source - prevent linting
# from complaining
assert ActionButtons is not None, "Needed to prevent linting failure"
assert Labels is not None, "Needed to prevent linting failure"
assert StatusFields is not None, "Needed to prevent linting failure"


class MotionControlApp(App):
    """Main class inheriting the App class of Kivy
    and is responsible for building the GUI."""

    main_screen = None

    def __init__(self, **kwargs):
        """Initialize class.

        On start this class can be initialized by adding fields underneath the
        super method. The super method must not be changed since that is likely to
        impact the functionality of the Kivy framework.
        """
        # We need to remove the debug flag so Kivy doesn't complain about an
        # unrecognised key
        if "debug" in kwargs:
            self.debug_mode = kwargs["debug"]
            kwargs.pop("debug")
        else:
            self.debug_mode = False

        # Load kivy files based on name of python module
        super(MotionControlApp, self).__init__(**kwargs)
        Builder.load_file(styles.__file__.split(".py")[0] + ".kv")
        Builder.load_file(settingswindow.__file__.split(".py")[0] + ".kv")
        Builder.load_file(infowindow.__file__.split(".py")[0] + ".kv")
        Builder.load_file(testwindow.__file__.split(".py")[0] + ".kv")
        Builder.load_file(mainscreen.__file__.split(".py")[0] + ".kv")

    def build(self) -> MainScreen:
        """Return the main screen layout"""
        return MainScreen(debug=self.debug_mode)
