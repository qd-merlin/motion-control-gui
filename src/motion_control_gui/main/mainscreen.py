"""Add functionality for main window.

Bind button functionality between interface and MotionLink options.
Update status fields.
"""

import os
import time
from pathlib import Path
from threading import Thread

from kivy.app import App
from kivy.clock import Clock, mainthread
from kivy.properties import AliasProperty, BooleanProperty, StringProperty
from kivy.uix.floatlayout import FloatLayout
from motion_control.controller.states import ConnectionState, CurrentState, RequestState
from motion_control.factory import get_configure_and_control_interface
from motion_control.util import Loggable

from motion_control_gui.modals.info.infowindow import InfoWindow
from motion_control_gui.modals.settings.settingswindow import SettingsWindow
from motion_control_gui.modals.test.testwindow import TestWindow


class MainScreen(FloatLayout, Loggable):
    """Add layout and functionality to the main app window."""

    # Main screen status fields
    connection_status = StringProperty("Disconnected")
    requested_state = StringProperty("Unknown")
    current_state = StringProperty("Unknown")
    interlock_type_label = StringProperty("Unknown")
    camera_in_msg = StringProperty("Unknown")
    camera_on_msg = StringProperty("Unknown")
    camera_veto_msg = StringProperty("Unknown")
    insert_position_mm = StringProperty("0")
    current_position_mm = StringProperty("0")
    title = StringProperty("Merlin motion control")

    # Other attributes
    trigger = StringProperty("Default")
    trig = BooleanProperty("False")

    interlocked = 0
    count = 0

    POLL_PERIOD = 0.1  # How frequently to update the GUI fields

    disable = False

    def __init__(self, debug: bool = False):
        """Create the main screen instance

        Args:
            zero_mq (bool, optional): Use the ZeroMQ interface to talk to the
                                      controller. Defaults to False.
            debug (bool, optional): Use debug mode with simulation.
                                    Defaults to False.
        """
        # Debug mode
        self.debug = debug

        # Only print the poll error message the first time and latch until the
        # controller has been connected again
        self.poll_error_latch = False

        # Initialise the motion controller interface
        self.control_interface = get_configure_and_control_interface(debug)

        # Window title and modal windows
        self.settings_window = SettingsWindow(self.control_interface)
        self.info_window = InfoWindow(self.title)
        self.test_window = TestWindow(self, self.control_interface)

        # Initialise the parent FloatLayout class
        super().__init__()

        # Kick off a thread to initialise the GUI values when connected to hardware
        self.init_thread = Thread(target=self.__initialise_gui_values, daemon=True)
        self.init_thread.start()

    def __initialise_gui_values(self):
        """Initialise the values in the GUI with actual hardware values"""
        # Wait for the connection to be established
        self.log_info_message(
            "Waiting for motion controller connection on "
            f"{self.control_interface.get_ip_address()}"
        )
        while (
            self.control_interface.get_connection_state()
            is ConnectionState.Disconnected
        ):
            time.sleep(3.0)

        self.log_info_message("Motion controller now connected")

        # Initialise the GUI values that aren't polled
        self.initialise_gui_fields()
        self.settings_window.update_fields()
        self.test_window.update_fields()

        # Kick off the main window polling thread
        Clock.schedule_interval(self.poll_task, MainScreen.POLL_PERIOD)

    @mainthread
    def initialise_gui_fields(self):
        """Initialise the GUI fields that are not regularly polled"""
        self.title = f"{self.control_interface.get_detector_name()} motion control"
        self.interlock_type_label = self.control_interface.get_interlock_type_label()
        self.requested_state = self.control_interface.get_current_state_name()

    @mainthread
    def update_gui_fields(self):
        """Called by the polling thread to update GUI fields"""
        try:
            self.current_position_mm = (
                f"{self.control_interface.get_current_position_mm():.2f} mm"
            )
            self.insert_position_mm = (
                f"{self.control_interface.get_insert_position_mm():.2f} mm"
            )
            self.current_state = self.control_interface.get_current_state_name()
            self.camera_in_msg = (
                "Yes" if self.control_interface.is_opposition_camera_in() else "No"
            )
            self.camera_on_msg = (
                "Yes" if self.control_interface.is_opposition_camera_on() else "No"
            )
            self.camera_veto_msg = (
                "Yes"
                if self.control_interface.is_opposition_camera_blocking()
                else "No"
            )
        except Exception:
            # Catch exceptions in case controller interface replies with None
            self.log_error_message("Could not update status fields")

    def disable_window(self) -> bool:
        """Whether to disable whole window interactions

        Returns:
            bool: True to disable, otherwise False
        """
        return True if self.connection_status == "Disconnected" else False

    def disable_standby_button(self) -> bool:
        """Whether to disable moving to the standby position button

        It should be disabled if already in position or moving

        Returns:
            bool: True to disable, otherwise False
        """
        if CurrentState[self.current_state] is (
            CurrentState.Moving or CurrentState.Standby
        ):
            return True
        else:
            return False

    def disable_insert_button(self) -> bool:
        """Whether to disable moving in button

        Returns:
            bool: True to disable, otherwise False
        """
        if CurrentState[self.current_state] is (
            CurrentState.Moving or CurrentState.Inserted
        ):
            return True
        else:
            return False

    def disable_retract_button(self) -> bool:
        """Whether to disable moving out button

        Returns:
            bool: True to disable, otherwise False
        """
        if CurrentState[self.current_state] is (
            CurrentState.Moving or CurrentState.Retracted
        ):
            return True
        else:
            return False

    def disable_stop_button(self) -> bool:
        """Whether to disable the stop button.

        Should only be visible when stage is moving.

        Returns:
            bool: True for disable, otherwise False
        """
        if CurrentState[self.current_state] is CurrentState.Moving:
            return False
        else:
            return True

    window_disabled = AliasProperty(disable_window, bind=["connection_status"])
    button_standby_disabled = AliasProperty(
        disable_standby_button, bind=["current_state", "trig"]
    )
    button_move_in_disabled = AliasProperty(
        disable_insert_button, bind=["current_state", "trig"]
    )
    button_move_out_disabled = AliasProperty(
        disable_retract_button, bind=["current_state", "trig"]
    )
    button_stop_disabled = AliasProperty(disable_stop_button, bind=["current_state"])

    @mainthread
    def standby(self):
        """Call move to standby."""
        self.count = 0
        self.trigger = "Pressed"

        if self.control_interface.standby():
            self.requested_state = RequestState.Standby.name
        else:
            self.requested_state = RequestState.Blocked.name

    @mainthread
    def move_in(self):
        """Call move in function on the MotionLink object."""
        self.count = 0
        self.trigger = "Pressed"

        if self.control_interface.insert():
            self.requested_state = RequestState.Inserted.name
        else:
            self.requested_state = RequestState.Blocked.name

    @mainthread
    def move_out(self):
        """Call move in function on the MotionLink object."""
        self.count = 0
        self.trigger = "Pressed"
        self.control_interface.retract()
        self.requested_state = RequestState.Retracted.name

    @mainthread
    def stop(self):
        """Call stop function on the MotionLink object."""
        self.requested_state = RequestState.Stopped.name
        self.control_interface.stop()

    def poll_task(self, *_):
        """Poll thread task to update the status fields on the main screen"""
        # Update the connection status field
        self.connection_status = self.control_interface.get_connection_state().name

        # If we are disconnected then just return without polling other fields
        if self.connection_status is ConnectionState.Disconnected.name:
            # Log the error message once and latch
            if self.poll_error_latch is False:
                self.log_error_message(
                    "Polling disabled whilst controller is disconnected"
                )
                self.poll_error_latch = True
            return
        else:
            # We are connected to controller so unlatch
            if self.poll_error_latch is True:
                self.log_info_message("Polling re-enabled")
                self.poll_error_latch = False

        # Update the status fields
        self.update_gui_fields()

        if self.trigger == "Pressed":
            self.trig = not self.trig

    def open_settings_window(self):
        """Open Settings window."""
        self.settings_window.open()
        self.settings_window.update_fields()

    def open_info_window(self):
        """Open Info window."""
        self.info_window.open()

    def open_test_window(self):
        """Open Info window."""
        self.test_window.open()
        self.test_window.update_fields()

    def get_application_icon_path(self) -> str:
        """Return the path to the application icon used in the GUI

        Used in the mainscreen.kv file.

        Returns:
            str: String of path to icon
        """
        views_dir = Path(os.path.dirname(__file__)).parent
        return f"{views_dir / 'static/images/white_atom.png'}"

    @mainthread
    def exit(self):
        """Exit the Kivy application"""
        App.get_running_app().stop()
