"""Class for adding functionality to Settings modal window."""

import datetime
import threading
import time
from time import sleep

from kivy.clock import Clock, mainthread
from kivy.properties import StringProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.modalview import ModalView
from kivy.uix.screenmanager import Screen, ScreenManager
from motion_control.controller import ControlInterface
from motion_control.controller.states import CurrentState
from motion_control.util import Loggable

# These classes are used in .kv file but not directly in this source
assert ScreenManager is not None, "Stop upsetting linting"
assert FloatLayout is not None, "Stop upsetting linting"
assert Screen is not None, "Stop upsetting linting"


class TestWindow(ModalView, Loggable):
    """Add functionality to Settings modal window."""

    cycles = StringProperty("200")
    cycle = StringProperty("Default")
    delay = StringProperty("10.0")
    insert_speed = StringProperty("20000")
    retract_speed = StringProperty("20000")
    insert_position = StringProperty("180000")
    total_test_time_label = StringProperty("Default")
    time_remaining_label = StringProperty("Default")
    time_estimate_label = StringProperty("Estimated time")
    total_test_time = 0
    time_remaining = 0
    new_time_estimate = 0
    clock_rate = 1
    running = False
    new_time = 0

    cycle_poll = 0.2

    def __init__(self, main_screen, control_interface: ControlInterface):
        """Create the test modal window

        Args:
            main_screen (MainScreen): Reference to main screen
            control_interface (ControlInterface): MotionLink interface
        """
        super().__init__()

        self.control_interface = control_interface
        self.main_screen = main_screen

        self.cycles = "200"
        self.delay = "10.0"

    def cycle_test(self):
        thrB = threading.Thread(target=self.start_cycle_test)
        thrB.start()

    def start_cycle_test(self):
        self.running = True
        self.new_time = 0
        thrA = threading.Thread(target=self.schedule_updater)
        thrA.start()
        self.cycle = "0"
        for cycle in range(int(self.cycles)):
            start_time = time.time()
            if self.running:
                # Move in
                self.cycle = str(cycle + 1)
                self.log_info_message(f"Starting cycle: {self.cycle}")
                self.main_screen.move_in()
                while (
                    CurrentState[self.main_screen.current_state]
                    is not CurrentState.Inserted
                ):
                    sleep(TestWindow.cycle_poll)

                # Wait for the delay
                sleep(float(self.delay))

                # Move out
                self.main_screen.move_out()
                while (
                    CurrentState[self.main_screen.current_state]
                    is not CurrentState.Retracted
                ):
                    sleep(TestWindow.cycle_poll)

                self.log_info_message(f"Completed cycle: {self.cycle}")

                # Wait for delay
                sleep(float(self.delay))
                elapsed_time = time.time() - start_time

                if self.new_time == 0:
                    self.new_time_estimate = float(elapsed_time)
                    time_new = int(
                        float(
                            self.new_time_estimate
                            * (int(float(self.cycles)) - int(float(self.cycle)))
                        )
                    )
                    time_total = int(
                        float(self.new_time_estimate * int(float(self.cycles)))
                    )
                    self.time_remaining = datetime.timedelta(seconds=time_new)
                    self.total_test_time = datetime.timedelta(seconds=time_total)
                    self.time_remaining_label = str(self.time_remaining)
                    self.total_test_time_label = str(self.total_test_time)
                    self.new_time = 1
            else:
                break

        self.cycle_test_timer.cancel()
        thrA.join()

    def schedule_updater(self):
        self.cycle_test_timer = Clock.schedule_interval(
            self.update_time_remaining, self.clock_rate
        )

    @mainthread
    def abort(self):
        self.running = False
        self.main_screen.requested_state = "Cycle Test Aborted"

    @mainthread
    def update_fields(self):
        try:
            # Calculate time required
            self.insert_speed = f"{self.control_interface.get_insert_speed_mm_per_s()}"
            self.retract_speed = (
                f"{self.control_interface.get_retract_speed_mm_per_s()}"
            )
            self.insert_position = f"{self.control_interface.get_insert_position_mm()}"

            if self.new_time == 1:
                time_per_cycle = self.new_time_estimate
            else:
                time_per_move_in = float(self.insert_position) / float(
                    self.insert_speed
                )
                time_per_move_out = float(self.insert_position) / float(
                    self.retract_speed
                )
                time_per_cycle = (
                    time_per_move_in + time_per_move_out + 2 * float(self.delay)
                )
            total_time = int(int(float(self.cycles)) * time_per_cycle)

            self.total_test_time = datetime.timedelta(seconds=total_time)
            self.total_test_time_label = str(self.total_test_time)
            self.time_remaining = self.total_test_time
            self.time_remaining_label = str(self.time_remaining)
        except Exception:
            # Catch exceptions in case controller interface replies with None
            self.log_error_message("Could not update test window fields")

    @mainthread
    def update_time_remaining(self, *args):
        self.time_remaining = self.time_remaining - datetime.timedelta(
            seconds=self.clock_rate
        )
        if self.time_remaining <= datetime.timedelta(seconds=0):
            self.time_remaining_label = str(datetime.timedelta(seconds=0))
        else:
            self.time_remaining_label = str(self.time_remaining)
