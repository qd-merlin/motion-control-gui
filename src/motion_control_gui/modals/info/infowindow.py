"""Class for adding functionality to the Info modal window."""

import webbrowser
from datetime import datetime

from kivy.properties import StringProperty
from kivy.uix.modalview import ModalView

from motion_control_gui import __version__


class InfoWindow(ModalView):
    """Add functionality to Info modal window."""

    copyright_date = StringProperty("Def")
    software_version = StringProperty("Def")
    software_title = StringProperty("Def")
    docs_url = StringProperty("Def")

    def __init__(self, title: str):
        """Initialize an fields for the Info modal window.

        Update copyright_date with today's year.
        Pull software version from MotionLink object.
        Pull software title from Motionlink object.

        On start this class can be initialized by adding fields underneath
        the super method. The super method must not be changed since that is
        likely to impact the functionality of the Kivy framework.
        """
        super().__init__()

        self.copyright_date = datetime.now().strftime("%Y")
        self.software_version = __version__
        self.software_title = title
        self.docs_url = "https://qd-merlin.gitlab.io/motion-control-gui/"

    def open_docs(self):
        """Open the documentation in a browser"""
        webbrowser.open(self.docs_url)
