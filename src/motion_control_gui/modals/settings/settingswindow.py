"""Class for adding functionality to Settings modal window."""

from kivy.clock import mainthread
from kivy.properties import StringProperty
from kivy.uix.modalview import ModalView
from motion_control.controller import ConfigureAndControlInterface
from motion_control.util import Loggable

from motion_control_gui.util import try_parse_value_string


class SettingsWindow(ModalView, Loggable):
    """Add functionality to Settings modal window."""

    window_title = StringProperty("Def")
    ip_address = StringProperty("Def")
    insert_speed = StringProperty("Def")
    retract_speed = StringProperty("Def")
    standby_position = StringProperty("Def")
    insert_position = StringProperty("Def")

    def __init__(self, control_interface: ConfigureAndControlInterface):
        """Create the settings modal window

        Args:
            control_interface (ControlInterface): Controller interface
        """

        super().__init__()

        self.window_title = "Controller properties"
        self.control_interface = control_interface

    def set_values(self):
        """Update the motion controller values with the values entered on the settings
        window.
        """
        # Parse and set values
        self.control_interface.set_ip_address(self.ip_address)

        insert_speed = try_parse_value_string(self.insert_speed, float)
        if insert_speed is not None:
            self.control_interface.set_insert_speed_mm_per_s(insert_speed)

        retract_speed = try_parse_value_string(self.retract_speed, float)
        if retract_speed is not None:
            self.control_interface.set_retract_speed_mm_per_s(retract_speed)

        insert_position = try_parse_value_string(self.insert_position, float)
        if insert_position is not None:
            self.control_interface.set_insert_position_mm(insert_position)

        standby_position = try_parse_value_string(self.standby_position, float)
        if insert_speed is not None:
            self.control_interface.set_standby_position_mm(standby_position)

        # Read back the values
        self.update_fields()

    @mainthread
    def update_fields(self):
        """Update the field values holding the motion controller properties"""
        try:
            self.ip_address = self.control_interface.get_ip_address()
            self.insert_speed = (
                f"{self.control_interface.get_insert_speed_mm_per_s():.2f}"
            )
            self.retract_speed = (
                f"{self.control_interface.get_retract_speed_mm_per_s():.2f}"
            )
            self.insert_position = (
                f"{self.control_interface.get_insert_position_mm():.2f}"
            )
            self.standby_position = (
                f"{self.control_interface.get_standby_position_mm():.2f}"
            )
        except Exception as e:
            # Catch exceptions in case controller interface replies with None
            self.log_error_message(f"Could not update status fields {e}")
