Contributing
============

When developing this project adhere to the following guidelines:

- Create a development branch from the main branch
- Use a suitable name to indicate what is changing, e.g.:
  
  - feature/<new_feature>
  - fix/<bug_name>

- Make changes and commit to this development branch
- Use linting tools to format the source code and check for syntax errors:

  - black
  - flake8
  - isort
  - mypy

- Test the changes with hardware and ensure everything works
- Once happy with the changes you should update the `CHANGELOG.rst` file with
  your changes and fixes
- If targeting a new release you should set `VERSION.txt` to the new version
- Create a merge request from the development branch to the main branch when the
  changes and linting checks are complete. Ideally have someone else review and
  test the changes.
- Once merged into the main branch you can then check out the new main branch,
  pull the changes and then create a tag matching the version in `VERSION.txt`
- After making a release you may want to make a new deployment package. See the
  README section "Deployment and Release" for instructions.

.. note::

  You may need to merge or rebase from the upstream main branch if multiple
  development branches are being worked on and merged at the same time.
