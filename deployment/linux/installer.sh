#!/usr/bin/bash

# Colours
CYAN="\033[0;36m"
GREEN="\033[1;32m"
YELLOW="\033[1;33m"
RED="\033[0;31m"
NOCOL="\033[0m"

PACKAGE_TITLE="Motion control GUI"


#================================================
# Script options
#================================================

script_dir="$( cd "$( dirname "$0" )" && pwd )"

# Store options from user
debug_flag=0
service_flag=0

options=":dhs"

while getopts ${options} flag
do
    case ${flag} in
        d)
            debug_flag=1
            ;;
        h)
            echo ""
            echo -e "==========================================================="
            echo "$PACKAGE_TITLE install script"
            echo -e "==========================================================="
            echo ""
            echo "This script is used to install the Motion Control GUI software."
            echo ""
            echo "Choose from the following options:"
            echo ""
            echo "    -d : build with debug output"
            echo "    -h : print this help message and exit"
            echo "    -s : create service file for ZeroMQ server"
            echo ""
            echo -e "==========================================================="
            echo ""
            exit 0
            ;;
        s)
            service_flag=1
            ;;
        ?)
            echo "ERROR: Invalid option '${OPTARG}'. Type -h for help"
            exit 1
            ;;
    esac
done


echo "==========================================================="
echo "Installing $PACKAGE_TITLE"
echo "==========================================================="
echo ""
echo -e "Package version: ${CYAN}$(basename ${script_dir})${NOCOL}"
echo ""

# Redirection for debug or minimal output
if [ $debug_flag -eq 1 ]; then
    echo -e "${GREEN}[INFO]${NOCOL} Enabling debug output"
    echo ""
    REDIRECT=/dev/stdout
else
    REDIRECT=/dev/null
fi

if [ $service_flag -eq 1 ]; then
    echo -e "${GREEN}[INFO]${NOCOL} Service file will be installed"
fi


#================================================
# Package configuration
#================================================

# Source files
source_docs_dir=$script_dir/docs
source_images_dir=$script_dir/images
source_service_dir=$script_dir/service
source_shortcut_directory=$script_dir/shortcut
source_wheels_dir=$script_dir/wheels

# Install paths
install_dir=/opt/QD/MotionControl

bin_dir=$install_dir/bin
config_dir=$install_dir/config
docs_dir=$install_dir/docs
images_dir=$install_dir/images
venv_dir=$install_dir/venv


#================================================
# Check for existing version
#================================================

if [ -d $venv_dir ]; then
    echo -e "${YELLOW}[WARNING]${NOCOL} Existing environment detected"
    while true; do
        read -p "$(echo -e 'Do you wish to replace the existing install?\n> ')" yn
        case $yn in
            [Yy]* )
                # Should replace all directories except for config
                echo -e "${CYAN} - Removing existing version${NOCOL}"
                rm -rf $bin_dir/*
                rm -rf $docs_dir
                rm -rf $images_dir
                rm -rf $venv_dir
                break
                ;;
            [Nn]* )
                echo -e "${YELLOW}[WARNING]${NOCOL} User aborted installation"
                exit 1
                ;;
            * )
                echo "Invalid input - please answer yes or no.";;
        esac
    done
fi


#================================================
# Create install paths
#================================================

echo -e "${CYAN} - Creating install paths${NOCOL}"

# Create top-level directory
sudo mkdir -p $install_dir

# Make current user own the installation directory
sudo chown -R $USER $install_dir
sudo chgrp -R $USER $install_dir

# Create subdirectories
mkdir -p $bin_dir
mkdir -p $config_dir
mkdir -p $docs_dir
mkdir -p $images_dir


#================================================
# Copy files
#================================================

echo -e "${CYAN} - Copying package files${NOCOL}"

cp -r $source_docs_dir/* $docs_dir
cp -r $source_images_dir/* $images_dir


#================================================
# Create installation environment
#================================================

echo -e "${CYAN} - Installing $PACKAGE_TITLE${NOCOL}"

# Create the virtual environment and check it was successful
python3.11 -m venv $venv_dir
source $venv_dir/bin/activate
if [ $? -ne 0 ]; then
    echo -e "${RED}[ERROR]${NOCOL} Failed to create and activate virtual environment"
    exit 1
fi

# Install the packages
pip install --upgrade pip > $REDIRECT
pip install $source_wheels_dir/*.whl > $REDIRECT
pip list installed > $REDIRECT


#================================================
# Creating binary links and desktop shortcut
#================================================

echo -e "${CYAN} - Creating application shortcuts${NOCOL}"

python_venv_bin=$venv_dir/bin

# Important entry points
check_config=$python_venv_bin/check_config
create_config=$python_venv_bin/create_config
generate_program_from_template=$python_venv_bin/generate_program_from_template
motion_control_gui=$python_venv_bin/motion_control_gui
motion_control_gui_console=$python_venv_bin/motion_control_gui_console  # Needed?
motion_control_version=$python_venv_bin/motion_control_version
upload_custom_program=$python_venv_bin/upload_custom_program
upload_generated_program=$python_venv_bin/upload_generated_program

# Create binary simlinks
ln -s $check_config $bin_dir/$(basename $check_config)
ln -s $create_config $bin_dir/$(basename $create_config)
ln -s $generate_program_from_template $bin_dir/$(basename $generate_program_from_template)
ln -s $motion_control_gui $bin_dir/$(basename $motion_control_gui)
ln -s $motion_control_gui_console $bin_dir/$(basename $motion_control_gui_console)
ln -s $motion_control_version $bin_dir/$(basename $motion_control_version)
ln -s $upload_custom_program $bin_dir/$(basename $upload_custom_program)
ln -s $upload_generated_program $bin_dir/$(basename $upload_generated_program)

# Copy launcher shortcuts
cp $source_shortcut_directory/* ~/.local/share/applications


#================================================
# Install service file
#================================================

if [ $service_flag -eq 1 ]; then
    echo -e "${CYAN} - Creating service file${NOCOL}"

    # Create a copy so we can substitute in values
    source_service_file=$source_service_dir/motion_control.service
    temp_service_file=$script_dir/motion_control.service

    cp $source_service_file $temp_service_file
    sed -i -e "s@%%user%%@${USER}@" $temp_service_file
    sed -i -e "s@%%group%%@${USER}@" $temp_service_file
    sed -i -e "s@%%home%%@${HOME}@" $temp_service_file

    sudo cp $temp_service_file /etc/systemd/system/

    rm $temp_service_file
fi

echo ""
echo -e "${GREEN}Installation complete${NOCOL}"
echo ""
