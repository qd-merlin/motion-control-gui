=============================
Motion control
=============================

This package contains the GUI application for controlling retractable
detector platforms from Quantum Detectors.

Dependencies
------------

These are based on using Ubuntu 22.04 or 24.04. Other distributions may
need different packages:

- Python 3.11 with venv (use deadsnakes if not available normally)
- xclip
- xsel

E.g. for Ubuntu 24.04 which includes Python 3.12 by default:

```
$ sudo apt-get install xclip xsel
$ sudo add-apt-repository ppa:deadsnakes/ppa
$ sudo apt update
$ sudo apt install python3.11 python3.11-dev
```


Installation
------------

Run the install.sh script inside this folder.

You can run it with -h to see the optional arguments such as installing
the ZeroMQ service file.


Install location
----------------

The root installation directory is:

- /opt/QD/MotionControl

The relevant contents of this directory are:

- /opt/QD/MotionControl/bin - Installed application shortcuts
- /opt/QD/MotionControl/config - The configuration for the motion controller
- /opt/QD/MotionControl/docs - Basic documentation for using the GUI

The bin folder could be added to the user's PATH to be able to use the commands
without providing the full path.


Application launcher
--------------------

A .desktop file is added to the user's application launcher directory:

- ~/.local/share/applications/

This means the GUI can be launched using the Activities launcher.


Using the service
-----------------

If you ran the install script with `-s` then the service file was created at:

- /etc/systemd/system/motion_control.service

This can be controlled using `systemctl` to start/stop/restart the service.

This provides a ZeroMQ server which controls the MotionLink controller based on
the configuration found in /opt/QD/MotionControl/config.

You can then use ZeroMQ clients to send control requests to this server to control
the stage. When the GUI is launched on the same machine it will automatically use
the ZeroMQ client if the `settings.json` file inside the config directory has been
flagged to use the ZeroMQ server.
