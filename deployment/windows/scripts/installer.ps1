# Script parameters
param (
    [switch]$offline = $false
)

# NOTE: if installing in offline mode then the Wheels directory should contain
#       all dependencies for an offline install. Otherwise install will fail.

# Add form support
Add-Type -AssemblyName System.Windows.Forms | Out-Null

Write-Output "==========================================================="
Write-Output "Installing Motion Control application"
Write-Output "==========================================================="

if ($offline -eq $true)
{
    Write-Output "- Installing in OFFLINE mode"
}
else
{
    Write-Output "- Installing in ONLINE mode"
}

#================================================
# Environment variables
#================================================

Set-Variable -Name "WINDOW_TITLE" -Value "Motion control installer                               "

# Set the install variables
Set-Variable -Name "INSTALL_DIR" -Value "C:\quantumdetectors\motion-control"
Set-Variable -Name "PACKAGE_DIR" -Value "$PSScriptRoot\..\"
Set-Variable -Name "SCRIPTS_DIR" -Value "$PACKAGE_DIR\scripts"
Set-Variable -Name "WHEEL_DIR" -Value "$PACKAGE_DIR\wheels"

$app_alias_error_message = @"
Installation failed - Found Windows app execution aliases.`n
Disable these aliases in Windows Settings by:`n
-  Searching for "Manage app execution aliases"`n
-  Look for "App Installer" linked from "python.exe"`n
-  Toggle the slider next to it off`n
-  Look for "App Installer" linked from "python3.exe"`n
-  Toggle the slider next to it off`n
-  Re-run this install script.`n
"@

$python_not_installed_message = @"
Installation failed - python and python3 commands failed.`n
Is Python installed and added to the system or user PATH?`n
"@


#================================================
# Python install check
#================================================

# Look for Python installation on PATH
try {
    # Try Python.exe
    [string] $output = Invoke-Expression -Command "python -V 2>&1"
    # Make sure we didn't find an app execution alias for the Microsoft App Store
    if ($output.Contains('Microsoft Store')) {
        # Found an alias to the app store installer - this may cause a problem
        Set-Variable -Name "FOUND_MICROSOFT_APP_ALIASES" -Value $true
    }
    else {
        Set-Variable -Name "PYTHON_COMMAND" -Value "python"
    }
}
Catch {
    # Don't return here as we will try Python3.exe first
}

if ((Test-Path variable:global:PYTHON_COMMAND) -eq $false) {
    # Python.exe didn't work so now try Python3.exe
    try {
        [string] $output = Invoke-Expression -Command "python3 -V 2>&1"
        # Make sure we didn't find an app execution alias for the Microsoft App Store
        if ($output.Contains('Microsoft Store')) {
            # Found an alias to the app store installer - this may cause a problem
            Set-Variable -Name "FOUND_MICROSOFT_APP_ALIASES" -Value $true
        }
        else {
            Set-Variable -Name "PYTHON_COMMAND" -Value "python3"
        }
    }
    Catch {
        # Python probably not installed
        [System.Windows.Forms.MessageBox]::Show(
            "$python_not_installed_message",
            "$WINDOW_TITLE",
            "Ok",
            "Error"
        )
        return
    }
}

# Check if a valid Python install was found
if ((Test-Path variable:global:PYTHON_COMMAND) -eq $false) {
    if (Test-Path variable:global:FOUND_MICROSOFT_APP_ALIASES) {
        # We found Microsoft app store execution aliases blocking us from finding Python
        [System.Windows.Forms.MessageBox]::Show(
            "$app_alias_error_message",
            "$WINDOW_TITLE",
            "Ok",
            "Error"
        )
    }
    else {
        # Not sure why we failed - Python may not be installed correctly or not on the user/system PATH
        [System.Windows.Forms.MessageBox]::Show(
            "$python_not_installed_message",
            "$WINDOW_TITLE",
            "Ok",
            "Error"
        )
    }
    return
}
else {
    Write-Output "- Using Python executable $PYTHON_COMMAND"
}


#================================================
# Host environment check
#================================================

# If doing an offline install check the version of Python matches the one used to create it
if ($offline -eq $true)
{
    [string] $output = Invoke-Expression -Command "python -V 2>&1"
    $PYTHON_VERSION = ($output -Split " ")[1]
    $OFFLINE_PYTHON_VERSION = Get-Content $SCRIPTS_DIR\PYTHON_VERSION.txt
    if ($PYTHON_VERSION -ne $OFFLINE_PYTHON_VERSION)
    {
        $answer = [System.Windows.Forms.MessageBox]::Show(
            "WARNING: Installed Python version $PYTHON_VERSION does not match deployment Python version $OFFLINE_PYTHON_VERSION. Proceed anyway?",
            "Python version mismatch",
            4,
            48
        )
        if ($answer -eq "No")
        {
            Write-Output "- Aborting install"
            $null = [System.Windows.Forms.MessageBox]::Show(
                "Installation aborted by user.",
                "$WINDOW_TITLE",
                "Ok",
                "Warning"
            )
            return
        }
    }
}

# Check if there is an existing installation
if (Test-Path $INSTALL_DIR) {
    $answer = [System.Windows.Forms.MessageBox]::Show(
        "Existing installation detected. Remove existing install to install this version?",
        "Existing installation",
        4,
        48
    )
    if ($answer -eq "Yes")
    {
        Write-Output "- Deleting existing install"
        Remove-Item -Path $INSTALL_DIR -Recurse
    }
    else
    {
        Write-Output "- Aborting install"
        $null = [System.Windows.Forms.MessageBox]::Show(
            "Installation aborted by user.",
            "$WINDOW_TITLE",
            "Ok",
            "Warning"
        )
        return
    }    
}


#================================================
# Package installation
#================================================

# Create the install directory
$null = New-Item -Path $INSTALL_DIR -ItemType Directory

# Create the Python environment and install the module
try
{
    # Create the virtual environment
    try {
        Write-Output "- Creating virtual environment"
        Invoke-Expression -Command "$PYTHON_COMMAND -m venv $INSTALL_DIR\venv"
    }
    catch {
        Write-Output "- Error creating virtual environment"
        throw
    }

    # Activate and optionally update the virtual environment
    try {
        Write-Output "- Activating virtual environment"
        Invoke-Expression -Command "$INSTALL_DIR\venv\Scripts\Activate.ps1"

        if ($offline -eq $false)
        {
            Write-Output "- Updating virtual environment pip version"
            $null = Invoke-Expression -Command "pip install --upgrade pip 2>&1"
        }
    }
    catch {
        Write-Output "- Error activating and updating virtual environment"
        throw
    }

    # Check if we are doing offline install
    if ($offline -eq $true)
    {
        # Offline install means we should have all dependencies inside the package already
        try {
            Write-Output "- [OFFLINE INSTALL] Installing downloaded dependency Wheels"
            Set-Variable -Name "WHEELS_REQUIRED" -Value "$SCRIPTS_DIR\wheels_required.txt"
            $null = Invoke-Expression -Command "pip install --no-index --find-links '$WHEEL_DIR' -r '$WHEELS_REQUIRED' --no-cache-dir 2>&1"
        }
        catch {
            Write-Output "- Error installing downloaded Wheels"
            throw
        }
    }

    # Install the backend motion_control module
    try {
        Write-Output "- Installing backend motion_control and motion_control_gui modules"
        Get-ChildItem $WHEEL_DIR -Filter motion_control*.whl | ForEach-Object {
            $WHEEL_FILE = $_.FullName
            if ($offline -eq $true)
            {
                # Offline install
                $null = Invoke-Expression -Command "pip install --no-index --find-links '$WHEEL_DIR' '$WHEEL_FILE' 2>&1"
            }
            else{
                # Online install
                $null = Invoke-Expression -Command "pip install '$WHEEL_FILE' --no-cache-dir 2>&1"
            }
        }
    }
    catch {
        Write-Output "- Error installing Wheel"
        throw
    }

    # Deactivate the environment
    Write-Output "- Deactivating environment"
    deactivate
}
Catch
{
    try{
        # Try and deactivate the active environment in case we failed part way
        # through installation.
        deactivate
    }
    Catch {
        # Catch the exception in the case the venv was never active
    }

    Write-Output "- Installation failed. Cleaning up install directory."
    Remove-Item -Path $INSTALL_DIR -Recurse
    $null = [System.Windows.Forms.MessageBox]::Show(
        "Installation failed. Error when creating virtual environment",
        "$WINDOW_TITLE",
        "Ok",
        "Error"
    )
    return
}

# Copy the images folder (contains shortcut icon)
Copy-Item -Recurse -Path "$PACKAGE_DIR\images" -Destination "$INSTALL_DIR\images"

# Create a GUI shortcut on the desktop (location may differ depending on user setup)
Write-Output "- Creating shortcuts"
Set-Variable -Name "SHORTCUT_TARGET" -Value "$INSTALL_DIR\venv\Scripts\motion_control_gui.exe"
if (Test-Path $Home\Desktop)
{
    Set-Variable -Name "SHORTCUT_LOCATION" -Value "$Home\Desktop\QD motion control.lnk"
    if (Test-Path "$SHORTCUT_LOCATION")
    {
        Remove-Item -Path $SHORTCUT_LOCATION
    }
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
    $Shortcut.TargetPath = $SHORTCUT_TARGET
    $shortcut.IconLocation = "$INSTALL_DIR\images\shortcut_icon.ico"
    $Shortcut.Save()
}
if (Test-Path $Home\OneDrive\Desktop)
{
    Set-Variable -Name "SHORTCUT_LOCATION" -Value "$Home\OneDrive\Desktop\QD motion control.lnk"
    if (Test-Path "$SHORTCUT_LOCATION")
    {
        Remove-Item -Path $SHORTCUT_LOCATION
    }
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
    $Shortcut.TargetPath = "$SHORTCUT_TARGET"
    $shortcut.IconLocation = "$INSTALL_DIR\images\shortcut_icon.ico"
    $Shortcut.Save()
}

# Create CLI shortcuts
Set-Variable -Name "SHORTCUT_LOCATION" -Value "$INSTALL_DIR\CreateConfig.lnk"
Set-Variable -Name "SHORTCUT_TARGET" -Value "$INSTALL_DIR\venv\Scripts\create_config.exe"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
$Shortcut.TargetPath = $SHORTCUT_TARGET
$Shortcut.Save()

Set-Variable -Name "SHORTCUT_LOCATION" -Value "$INSTALL_DIR\CheckConfig.lnk"
Set-Variable -Name "SHORTCUT_TARGET" -Value "$INSTALL_DIR\venv\Scripts\check_config.exe"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
$Shortcut.TargetPath = $SHORTCUT_TARGET
$Shortcut.Save()

Set-Variable -Name "SHORTCUT_LOCATION" -Value "$INSTALL_DIR\UploadGeneratedProgram.lnk"
Set-Variable -Name "SHORTCUT_TARGET" -Value "$INSTALL_DIR\venv\Scripts\upload_generated_program.exe"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
$Shortcut.TargetPath = $SHORTCUT_TARGET
$Shortcut.Save()

Set-Variable -Name "SHORTCUT_LOCATION" -Value "$INSTALL_DIR\UploadCustomProgram.lnk"
Set-Variable -Name "SHORTCUT_TARGET" -Value "$INSTALL_DIR\venv\Scripts\upload_custom_program.exe"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
$Shortcut.TargetPath = $SHORTCUT_TARGET
$Shortcut.Save()

Set-Variable -Name "SHORTCUT_LOCATION" -Value "$INSTALL_DIR\GenerateProgramFromTemplate.lnk"
Set-Variable -Name "SHORTCUT_TARGET" -Value "$INSTALL_DIR\venv\Scripts\generate_program_from_template.exe"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
$Shortcut.TargetPath = $SHORTCUT_TARGET
$Shortcut.Save()

Set-Variable -Name "SHORTCUT_LOCATION" -Value "$INSTALL_DIR\MotionControlGuiWithConsole.lnk"
Set-Variable -Name "SHORTCUT_TARGET" -Value "$INSTALL_DIR\venv\Scripts\motion_control_gui_console.exe"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut($SHORTCUT_LOCATION)
$Shortcut.TargetPath = $SHORTCUT_TARGET
$Shortcut.Save()


#================================================
# Installation check and finish
#================================================

# Sanity check that installation worked
try {
    Write-Output "- Testing application"
    Invoke-Expression -Command "$INSTALL_DIR\venv\Scripts\motion_control_gui.exe version"
}
catch {
    Write-Output "- Installation failed. Cleaning up install directory."
    Remove-Item -Path $INSTALL_DIR -Recurse
    $null = [System.Windows.Forms.MessageBox]::Show(
        "Installation failed.`nCould not launch motion control application",
        "$WINDOW_TITLE",
        "Ok",
        "Error"
    )
    return
}

# Finished!
$null = [System.Windows.Forms.MessageBox]::Show(
    "Installation successful.",
    "$WINDOW_TITLE",
    "Ok",
    "Info"
)
Write-Output "- Install completed!"
return
