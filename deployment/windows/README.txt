==============
Motion control
==============

This package contains the GUI application for controlling retractable
detector platforms from Quantum Detectors.

Installation
------------

Run the install.bat script inside this folder.

This will install the application on the host PC and create a desktop shortcut
for launching the GUI application.
