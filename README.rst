Motion control GUI application
==============================

|code_ci| |coverage| |release|

This is the Python GUI application for controlling the Quantum Detectors
retractable detector stages.

It uses the Motion Control backend Python module to communicate with the
motion controller.


Project structure
-----------------

The project is laid out in the following folder structure:

- dependencies - contains the `motion_control` Wheel dependency
- deployment - contains files required in the built deployment package
- docs - contains user documentation source files built by Sphinx
- images - image source files
- scripts - contains the deployment script to create the deployment package
- src - contains the Python module source code
- tests - contains unit test modules

This project uses `pyproject.toml` to describe the Python package.

During development/building additional folders may appear:

- build - can be created when building the Python Wheel or other distributable
- dist - contains the built Python distributables
- deploy - contains the built deployment package created from the deploy script


Requirements
------------

The following software is required for running the application:

Windows:

- Python 3

Linux:

- Python 3
- xclip (needed for Kivy)
- xsel (needed for Kivy)

Most Python module dependencies are specified in the `pyproject.toml` file and
are installed automatically when this module is installed.

It also depends on the QD Python module `motion_control`. A stable version of
this module is provided in `dependencies`. Installing a later version or in
an editable mode may require updating the interfaces to the module.

.. note::

  Python 3.12 is not yet fully supported. It is advised to use Python 3.11.


Contributing
------------

To contribute to this project follow the guidelines in `CONTRIBUTING`_.

You can follow the development instructions below to create a development
environment for testing and making changes to the project source.


Development
-----------

Windows
#######

You can use `pipenv` to create and manage the virtual environment for this
project.

If you do not have `pipenv` installed you can grab it from PyPI:

.. code-block:: pwsh-session

  PS > pip install -U pipenv


Then the development environment including all development dependencies can be
installed by running:

.. code-block:: pwsh-session

  PS > pipenv install --dev

This will install the module dependencies under `dev` in
`project.optional-dependencies` in the `pyproject.toml` file.


Or for runtime dependencies only (`dependencies` listed in the `pyproject.toml`):

.. code-block:: pwsh-session

  PS > pipenv install


If you have any issues trying to install the dependencies you can try installing
them without using the Pipfile.lock file. This can happen if using different
versions of Python and trying to resolve the dependencies and versions:

.. code-block:: pwsh-session

  PS > pipenv install --dev --skip-lock


The `Pipfile` is configured to install this module as an editable package. This
allows you to modify the source Python files and the changes will be reflected
the next time any of the entry points are run.

The `motion_control` module needs to be installed manually when developing the
module. The latest compatible and tested release can be found in the
`dependencies` folder in this repository. This can be installed with the
command `pipenv run pip install <module_wheel_file>`, e.g.:

.. code-block:: pwsh-session

  PS > pipenv run pip install .\dependencies\motion_control-1.0.0-py3-none-any.whl

If you are working on or testing changes to this backend module then see the
subsection below on how to install the source files as an editable module.


Linux
#####

The following steps are based on Ubuntu 22.04. It should also work on Ubuntu
24.04 with Python 3.12.

If not already installed, install `pipenv`:

.. code-block:: bash

  $ sudo apt-get install pipenv


Then the development environment including all development dependencies can be
installed by running:

.. code-block:: bash

  $ pipenv install --dev --skip-lock


This will install the module dependencies under `dev` in
`project.optional-dependencies` in the `pyproject.toml` file. We use the
`--skip-lock` argument here as the `Pipfile.lock` does not support multiple
platforms and this was generated from Windows.


Or for runtime dependencies only (`dependencies` listed in the `pyproject.toml`):

.. code-block:: bash

  $ pipenv install --skip-lock


The `Pipfile` is configured to install this module as an editable package. This
allows you to modify the source Python files and the changes will be reflected
the next time any of the entry points are run.

The `motion_control` module needs to be installed manually when developing the
module. The latest compatible and tested release can be found in the
`dependencies` folder in this repository. This can be installed with the
command `pipenv run pip install <module_wheel_file>`, e.g.:

.. code-block:: bash

  $ pipenv run pip install .\dependencies\motion_control-1.0.0-py3-none-any.whl

If you are working on or testing changes to this backend module then see the
subsection below on how to install the source files as an editable module.


Working on and testing the backend module
#########################################

If you want to make changes to the `motion_control` module at the same time
you can install the module source in editable mode. This lets you update the
source code and have the changes applied in the environment where it is
installed without having to rebuild and reinstall from a Wheel.

The following is the example installation command assuming that you are in the
root directory of this repository and the `motion_control` repository is
located within the same parent folder:

.. code-block:: pwsh-session

  PS > pipenv run pip install -e ..\motion-control\

The `-e` argument means "editable install" and this is followed by the relative
path from the current working directory (i.e. root of this repository) to the
`motion_control` repository root directory (where the `pyproject.toml` file
is located).

Once the backend module has been updated and released then you should replace
the Wheel in the `dependencies/` folder with the new version, either from the
CI job or building it yourself using `pipenv run build` in the backend module
repository directory and then copying it from `dist/`. You may want to test
this new Wheel in the development environment using by uninstalling the
editable version (`pipenv run pip uninstall motion_control`) and then installing
the new version from the wheel (`pipenv run pip install <wheel_file>`).


Deployment and Release
----------------------

When creating a new release you should have created a new tag and updated
the version in `VERSION.txt` from following the `CONTRIBUTING`_ guidelines.

When pushing the tag to GitLab the CI deploy job will run, which creates a
deployment package by running the script `./scripts/deploy.bat`. Released
packages can be found on this project's `GitLab package registry`_.

You can test creating a deployment package locally by running the
`./scripts/deploy.bat` yourself. It will create a `.zip` package containing
an install script and other files required for installation. The name of
the package will be based on the current version set in `VERSION.txt`.


Offline deployment packages
###########################

As of version 3.1.6 the `deploy.bat` script supports creation of an offline
deployment package. This will download all dependencies and place them in
the package's `Wheels` directory. Run with the `--offline` flag to create an
offline deployment:

.. code-block:: pwsh-session

  PS > .\scripts\deploy.bat --offline

An additional batch file `install_offline.bat` will be copied into the
deployment package which passes the `--offline` flag to the install script
to indicate that it should install the downloaded dependencies and not use
the package index.

You should create this offline package with the desired version of Python as
this can affect the versions of Wheels downloaded and their compatibility with
other versions of Python. A warning will be presented to the user if they try
an offline install with a non-matching version of Python.

The online `install.bat` script also remains in the package to keep an online
install option available (without having to modify the batch/PowerShell script).


Installation
------------

The application can be installed in two modes depending on whether the host PC
has access to the internet (for downloading the Python dependencies). A recent
version of Python 3 should already be installed.

Online install
##############

The following instructions are for installing an online version (i.e. the
target PC has internet access for Python dependencies).

- Download the desired release version from the `package registry`_
- Extract the contents of the `.zip` file
- Run the `install.bat` script by double clicking on it

You may get prompts as it runs if it detects a previous install or a problem
occurs with the installation.

This will install the module in a virtual environment in
`C:/quantumdetectors/motion_control` and create a desktop shortcut to launch the
GUI application. There will also be CLI shortcuts created in
`C:/quantumdetectors/motion-control` to each CLI application:

- `CheckConfig` - checking existing configuration file
- `CreateConfig` - creating configuration files
- `UpdateInterlock` - updating the motion program on the controller

See Usage section below for more information.


Offline install
###############

The following instructions are for install an offline version where the target
PC has no internet access. You should ideally use an offline release package
created using the same version of Python running on the target PC.

- Create the offline release package: see "Offline deployment packages" above
- Copy the created offline deployment package to target PC
- Extract the package and run `install_offline.bat` by double-clicking it

You may get prompts as it runs if it detects a previous install or a problem
occurs with the installation.

This will install the module in a virtual environment in
`C:/quantumdetectors/motion_control` and create a desktop shortcut to launch the
GUI application. There will also be CLI shortcuts created in
`C:/quantumdetectors/motion-control` to each CLI application:

- `CheckConfig` - checking existing configuration file
- `CreateConfig` - creating configuration files
- `UpdateInterlock` - updating the motion program on the controller

See Usage section below for more information.


Usage
-----

If you are using a release version then in the installed Python environment the
following entry point is available for launching the GUI application:

- motion_control_gui.exe - used to launch the GUI application


Or in a development environment you can run this with `pipenv`:

.. code-block:: pwsh-session

    PS > pipenv run motion_control_gui


By default the output console will be minimised once the GUI application has
connected successfully to the motion controller.

The other entry points come from the backend `motion_control` Python module.


Creating the configuration file
###############################

The configuration settings file is read from the following path:

.. code-block:: pwsh-session

    C:\ProgramData\MotionControl\settings.json

If this path does not exist when running `update_interlock` or `motion_control`
then a default file will be created. You can then modify this manually.

You can also use `create_config` to guide you through creating the
configuration file, including overwriting an existing file. Note that the
override interlock type is hidden in the user prompt but still exists (enter 0
to select it).

This file will contain the following properties:

- IP address of the motion controller
- Type of detector mounted
- Interlock type being used
- Insert speed
- Insert position
- Retract position
- Standby position
- Maximum allowed position
- IP address of the ZeroMQ UDI server (optional - used for the Thermo Fisher UDI
  collision prevention client)

You can validate an existing file using `check_config`. A validator will check
the file against a JSON schema to see if it has the required fields and the
provided values are correct. Note that it will not error if it detects an
unrecognised field (for forwards and backwards compatibility).

Every time the configuration file is updated you should run `update_interlock`
to ensure that the state of the motion controller is consistent with what the
GUI application expects.


Using the simulator
###################

You can run the GUI application against a simulator instead of a real controller
using the `\-\-debug` argument. Note that the additional `\-\-` is required to
tell Kivy to ignore the argument as it has its own argument parser.

Using this option will not use the ZeroMQ client interface, even if configured.

The connection status should say "Debug" if the application is running in this
mode. This will also be true the ZeroMQ server is running in simulation mode but
this GUI application is not.

.. code-block:: pwsh-session

    PS > motion_control_gui.exe -- --debug
    PS > pipenv run motion_control_gui -- --debug


Documentation
-------------

User-oriented documentation is provided using Sphinx. This can be built locally
using `pipenv`:

.. code-block:: pwsh-session

    PS > pipenv run docs


Documentation will be built locally in `docs/build/html`. Open `index.html` in a
browser to see it.

A CI/CD job will deploy built user documentation to GitLab pages when the main
branch is updated.

You can view the documentation `here`_.


.. _CONTRIBUTING:
  https://gitlab.com/qd-merlin/motion-control-gui/-/blob/master/CONTRIBUTING.rst

.. _CHANGELOG:
  https://gitlab.com/qd-merlin/motion-control-gui/-/blob/master/CHANGELOG.rst

.. _here:
  https://qd-merlin.gitlab.io/motion-control-gui/

.. _GitLab package registry:
  https://gitlab.com/qd-merlin/motion-control-gui/-/packages

.. _package registry:
  https://gitlab.com/qd-merlin/motion-control-gui/-/packages

.. |code_ci| image:: https://gitlab.com/qd-merlin/motion-control-gui/badges/master/pipeline.svg?ignore_skipped=true
  :target: https://gitlab.com/qd-merlin/motion-control-gui/-/pipelines
  :alt: Code CI

.. |coverage| image:: https://gitlab.com/qd-merlin/motion-control-gui/badges/master/coverage.svg?ignore_skipped=true
  :target: https://gitlab.com/qd-merlin/motion-control-gui/-/commits/master
  :alt: Coverage

.. |release| image:: https://gitlab.com/qd-merlin/motion-control-gui/-/badges/release.svg
  :target: https://gitlab.com/qd-merlin/motion-control-gui/-/releases
  :alt: Latest release
