import sys
from unittest.mock import patch

from motion_control_gui.cli.motion_control import main


def test_motion_control_version_arg():
    args_with_version = sys.argv + ["version"]
    with patch.object(sys, "argv", args_with_version):
        main()
