from threading import Thread
from time import sleep

import pytest
from kivy.base import EventLoop

from motion_control_gui.app import MotionControlApp


@pytest.mark.skip(
    reason="Does not run on CI server - need to find out how to mock backend"
)
def test_app():
    app = MotionControlApp(debug=True)

    def run_app():
        app.run()

    # Start the app in a background thread
    app_thread = Thread(target=run_app, daemon=True)
    app_thread.start()

    # Wait for Kivy Event Loop to start running
    timeout = 10.0
    waited = 0.0
    while EventLoop.status != "started":
        sleep(0.1)
        waited += 0.1

        if app_thread.is_alive() is False:
            assert False, "Application thread not running"
        elif waited > timeout:
            assert False, "Timeout waiting for application to start"

    # Wait 1 more second for simulation controller polling to start
    sleep(1)

    # Check that the motion controller simulator is connected
    assert app.main_screen.control_interface.debug is True
    assert app.main_screen.connection_status == "Debug"

    app.main_screen.exit()
    app_thread.join()
